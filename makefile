
#@_@   pthread_test_A 
#WWY-2021-11-05

#指定C文件编译器
CC = gcc
#指定临时文件路径
TEMP_DIR=./temp/
#指定最终程序文件路径
BIN_DIR=./bin/
#指定编译后临时文件
object=$(TEMP_DIR)temp.o
#指定编译链接后的最终程序文件
target=$(BIN_DIR)test_A.out

#	@在命令行首部，可屏蔽命令输出回显


#	对pthread的库有依赖，所以添加 -lpthread
new_app:$(object)
	$(CC) -o $(target) $(object) -lpthread

$(object):pthread_test_A.c
	$(CC) -c pthread_test_A.c  -o $(object)
	
#伪装文件命令
.PHONY:clean

#创建清理内容
clean:
	@-echo "---------CLEAN--------"
	@-rm -rf $(TEMP_DIR)*.o
	@-rm -rf $(BIN_DIR)*.out
