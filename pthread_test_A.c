//这两个头文件不多说
#include <stdio.h>
#include <unistd.h>
#include <string.h>

//pthread的头文件
#include <pthread.h>
//用于获取微秒级的时间
#include <sys/time.h>


//本例程可通过gcc命令编译---CMD:"gcc ./pthread_test_A.c -lpthread  -o ./AA"




/*
pthread 主要操作
        对象	    操作	    
    线程
        创建	pthread_create
        退出	pthread_exit	
        等待	pthread_join	
    互斥锁
        创建	pthread_mutex_init	
        销毁	pthread_mutex_destroy	
        加锁	pthread_mutex_lock	
        解锁	pthread_mutex_unlock	
    条件
        创建	pthread_cond_init	
        销毁	pthread_cond_destroy	
        触发	pthread_cond_signal	
        广播	pthread_cond_broadcast	
        等待	pthread_cond_wait / pthread_cond_timedwait
其他操作
        用于返回当前进程的ID                 pthread_t pthread_self (void);
        线程资源释放，必须成对出现             pthread_cleanup_push/pthread_cleanup_pop
*/



/*
--------基于C语言和pthread程的序设计---------
程序设计要求：
    0、设计主线程外含有三个线程的程序
        0-要求主线程和三个线程之间可进行数据交互，主线程执行周期内写数据，各子线程获取字符数据长度即可
        1-要求各线程可取消并可安全退出
        2-要求数据读写安全
    1、建立线程安全策略
*/

//可尝试开关（0-1）下列宏定义观察操作执行
//打印数据
#define PRINT_DATA              0   
//资源锁启用
#define MUTEX_ENABLE            1 
//主线程结束时取消其他线程
#define CANCLE_ENABLE           1
//取消其他线程时清理各个线程未释放的资源
#define CANCLE_CLEAR_ENABLE     1 

//声明三个线测试程函数
int thread_function_A(const char * data );
int thread_function_B(const char * data );
int thread_function_C(const char * data );

//声明对应的三个线程清理函数，打印线程取消信息
void thread_function_A_clean_up_1(void * arg);
void thread_function_B_clean_up_1(void * arg);
void thread_function_C_clean_up_1(void * arg);
//声明对应的三个线程清理函数，防止线程退出时资源未被释放，在下列函数中执行锁释放
void thread_function_A_clean_up_2(void * arg);
void thread_function_B_clean_up_2(void * arg);
void thread_function_C_clean_up_2(void * arg);

//创建线程ID
pthread_t thread1,thread2,thread3;
//创建3个线程各自的数据参数
char data_A[128]="PAR-A";
char data_B[128]="PAR-B";
char data_C[128]="PAR-C";

//创建锁
pthread_mutex_t mutex_A=PTHREAD_MUTEX_INITIALIZER; //编译时初始化锁位解锁状态;
pthread_mutex_t mutex_B=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_C=PTHREAD_MUTEX_INITIALIZER;;


int main()
{
    printf("------->MUTI-THREAD-PTHREAD-TEST-A<--------\r\n");

    struct timeval start_time,stop_time,time_1,time_2,time_3;

//初始化解锁
    pthread_mutex_init(&mutex_A,NULL);
    pthread_mutex_init(&mutex_B,NULL);
    pthread_mutex_init(&mutex_C,NULL);

//创建线程
    const int res1=pthread_create(&thread1,NULL,(void*)(&thread_function_A),(void*)data_A);
    const int res2=pthread_create(&thread2,NULL,(void*)(&thread_function_B),(void*)data_B);
    const int res3=pthread_create(&thread3,NULL,(void*)(&thread_function_C),(void*)data_C);

//记录时间-可定位到微秒级
    gettimeofday(&start_time,NULL);
    if(1)
    {
        int count=0;
        while (count<6000)
        {
            /* code */
            const pthread_t  p_id=pthread_self();
            count++;
            if (MUTEX_ENABLE)
            {
                //记录每个周期内各个线程数据赋值状态
                int over_A=0;
                int over_B=0;
                int over_C=0;

                //主线程要和三个线程通信，数据交互时尽量采用状态机形式，使用pthread_mutex_trylock(&mutex)进行非阻塞式轮询
                while (1)
                {
                    /* code */
                    if(over_A==0)
                    {
                        if(pthread_mutex_trylock(&mutex_A)==0)
                        {
                            snprintf(data_A,127,"MAIN-PID:%016lu---COUNT:%010d",p_id,count);
                            pthread_mutex_unlock(&mutex_A);
                            over_A=1;
                        }
                    }
                    if(over_B==0)
                    {
                        if(pthread_mutex_trylock(&mutex_B)==0)
                        {
                            snprintf(data_B,127,"MAIN-PID:%016lu---COUNT:%010d",p_id,count);
                            pthread_mutex_unlock(&mutex_B);
                            over_B=1;
                        }
                    }
                     if(over_C==0)
                    {
                        if(pthread_mutex_trylock(&mutex_C)==0)
                        {
                            snprintf(data_C,127,"MAIN-PID:%016lu---COUNT:%010d",p_id,count);
                            pthread_mutex_unlock(&mutex_C);
                            over_C=1;
                        }
                    }

                    if((over_A&over_B&over_C)!=0)
                    {
                        break;
                    }
                }
                
            }
            else
            {
                snprintf(data_A,127,"MAIN-PID:%016lu---COUNT:%010d",p_id,count);
                snprintf(data_B,127,"MAIN-PID:%016lu---COUNT:%010d",p_id,count);
                snprintf(data_C,127,"MAIN-PID:%016lu---COUNT:%010d",p_id,count);
            }
            usleep(1);
        }   
    }
    gettimeofday(&stop_time,NULL);

    //打印线程创建结果，0为正常

    int re_A=0;
    int re_B=0;
    int re_C=0;


#if CANCLE_ENABLE
    const int cancle_1=pthread_cancel(thread1);
    const int cancle_2=pthread_cancel(thread2);
    const int cancle_3=pthread_cancel(thread3);
#endif

    //等待线程结束并获取时间
    //pthread_join可获取线程函数返回值并释放线程空间,但会阻塞当前线程，成功返回0
    pthread_join(thread1,(void**)(&re_A));
    gettimeofday(&time_1,NULL);
    //等待线程结束并获取时间
    pthread_join(thread2,(void**)(&re_B));
    gettimeofday(&time_2,NULL);
    //等待线程结束并获取时间
    pthread_join(thread3,(void**)(&re_C));
    gettimeofday(&time_3,NULL);



    //尝试资源锁
    const int try_lock_A=pthread_mutex_trylock(&mutex_A);
    const int try_lock_B=pthread_mutex_trylock(&mutex_B);
    const int try_lock_C=pthread_mutex_trylock(&mutex_C);
    //释放尝试OK的资源锁
    if(try_lock_A==0)
    {
        pthread_mutex_unlock(&mutex_A);
    }
    if(try_lock_B==0)
    {
        pthread_mutex_unlock(&mutex_B);
    }
    if(try_lock_C==0)
    {
        pthread_mutex_unlock(&mutex_C);
    }


    //销毁资源锁
    pthread_mutex_destroy(&mutex_A);
    pthread_mutex_destroy(&mutex_B);
    pthread_mutex_destroy(&mutex_C);


    printf("PTHREAD_CREATE_RES  \t:\t_1_%010d---\t_2_%010d---\t_3_%010d-\r\n",res1,res2,res3);
    printf("PTHREAD_JOIN_RES    \t:\t_1_%010d---\t_2_%010d---\t_3_%010d-\r\n",re_A,re_B,re_C);

    printf("PTHREAD_MUTEX_TRY   \t:\t_1_%010d---\t_2_%010d---\t_3_%010d-\r\n",try_lock_A,try_lock_B,try_lock_C);


#if CANCLE_ENABLE
    printf("PTHREAD_CANCLE_RES  \t:\t_1_%010d---\t_2_%010d---\t_3_%010d-\r\n",cancle_1,cancle_2,cancle_3);
#endif


    printf("PTHREAD_START_TIME  \t:S:%016ld---U:%07ld\r\n",start_time.tv_sec,start_time.tv_usec);
    printf("PTHREAD_STOP_TIME   \t:S:%016ld---U:%07ld\r\n",stop_time .tv_sec,stop_time .tv_usec);

    printf("PTHREAD_1_TIME      \t:S:%016ld---U:%07ld\r\n",time_1 .tv_sec,time_1 .tv_usec);
    printf("PTHREAD_2_TIME      \t:S:%016ld---U:%07ld\r\n",time_2 .tv_sec,time_2 .tv_usec);
    printf("PTHREAD_3_TIME      \t:S:%016ld---U:%07ld\r\n",time_3 .tv_sec,time_3 .tv_usec);

    printf("START--->STOP       \t:U:%016ld\r\n",(stop_time  .tv_sec-start_time.tv_sec)*1000*1000+(stop_time .tv_usec-start_time.tv_usec));
    printf("START--->A_END      \t:U:%016ld\r\n",(time_1     .tv_sec-start_time.tv_sec)*1000*1000+(time_1    .tv_usec-start_time.tv_usec));
    printf("START--->B_END      \t:U:%016ld\r\n",(time_2     .tv_sec-start_time.tv_sec)*1000*1000+(time_2    .tv_usec-start_time.tv_usec));
    printf("START--->C_END      \t:U:%016ld\r\n",(time_3     .tv_sec-start_time.tv_sec)*1000*1000+(time_3    .tv_usec-start_time.tv_usec));

    return 0;
}


int thread_function_A(const char * data )
{
    //设置线程接受取消信号，默认即为接受取消状态（PTHREAD_CANCEL_ENABLE，PTHREAD_CANCEL_DISABLE）
    pthread_setcancelstate  (PTHREAD_CANCEL_ENABLE      ,NULL);
    //设置线程接受取消信号后的取消类型，PTHREAD_CANCEL_DEFERRED-到取消点再执行退出，PTHREAD_CANCEL_ASYNCHRONOUS-立即退出（PTHREAD_CANCEL_DEFERRED，PTHREAD_CANCEL_ASYNCHRONOUS）
    pthread_setcanceltype   (PTHREAD_CANCEL_DEFERRED    ,NULL);
    //部分阻塞的系统调用本身即为取消点

    int count=0;
    int data_length=0;

    pthread_cleanup_push((void *)thread_function_A_clean_up_1,"CLEAN-1");
    pthread_cleanup_push((void *)thread_function_A_clean_up_1,"CLEAN-2");
    pthread_cleanup_push((void *)thread_function_A_clean_up_2,"CLEAN-1");
    pthread_cleanup_push((void *)thread_function_A_clean_up_2,"CLEAN-2");
    printf("THREAD_A-%016lu-START--\r\n",pthread_self());

    while (count<12000)
    {
        /* code */
        const pthread_t  p_id=pthread_self();
        if(MUTEX_ENABLE)
        {
            pthread_mutex_lock(&mutex_A);

        }
        //手动添加取消点--在资源上锁时执行取消测试，一旦在此处执行取消，锁的状态依然保持，锁资源由资源回收函数解锁;当然，由于usleep()的存在，大概率在usleep里执行取消操作，而非此处
        pthread_testcancel();
        const int length=strlen(data);
        if(PRINT_DATA)
        {
            printf("THREAD_A-%016lu-DATA:-_-%010d-_-%s\r\n",p_id,count,data);
        }
        if(length!=data_length)
        {
            printf("THREAD_A-DATA-LENGTH-CHANGED:%d\r\n",length);
            data_length=length;
        }
        if(MUTEX_ENABLE)
        {
            pthread_mutex_unlock(&mutex_A);
        }

        count++;
        usleep(1);
        
    }   

    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);

    return count;
}
int thread_function_B(const char * data )
{
    //设置线程接受取消信号，默认即为接受取消状态（PTHREAD_CANCEL_ENABLE，PTHREAD_CANCEL_DISABLE）
    pthread_setcancelstate  (PTHREAD_CANCEL_ENABLE      ,NULL);
    //设置线程接受取消信号后的取消类型，PTHREAD_CANCEL_DEFERRED-到取消点再执行退出，PTHREAD_CANCEL_ASYNCHRONOUS-立即退出（PTHREAD_CANCEL_DEFERRED，PTHREAD_CANCEL_ASYNCHRONOUS）
    pthread_setcanceltype   (PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
    //部分阻塞的系统调用本身即为取消点

    int count=0;
    int data_length=0;

    pthread_cleanup_push((void *)thread_function_B_clean_up_1,"CLEAN-1");
    pthread_cleanup_push((void *)thread_function_B_clean_up_1,"CLEAN-2");
    pthread_cleanup_push((void *)thread_function_B_clean_up_2,"CLEAN-1");
    pthread_cleanup_push((void *)thread_function_B_clean_up_2,"CLEAN-2");
    printf("THREAD_B-%016lu-START--\r\n",pthread_self());

    while (count<24000)
    {
        /* code */
        const pthread_t  p_id=pthread_self();
        if(MUTEX_ENABLE)
        {
            pthread_mutex_lock(&mutex_B);
        }
        const int length=strlen(data);
        if(PRINT_DATA)
        {
            printf("THREAD_B-%016lu-DATA:-_-%010d-_-%s\r\n",p_id,count,data);
        }
        if(length!=data_length)
        {
            printf("THREAD_B-DATA-LENGTH-CHANGED:%d\r\n",length);
            data_length=length;
        }
        if(MUTEX_ENABLE)
        {
            pthread_mutex_unlock(&mutex_B);
        }
        //手动添加取消点，本线程设置为取消后立即退出故取消点并多少无意义
        pthread_testcancel();
        count++;
        usleep(1);
    }    

    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);

    return count;
}
int thread_function_C(const char * data )
{
    //设置线程接受取消信号，默认即为接受取消状态（PTHREAD_CANCEL_ENABLE，PTHREAD_CANCEL_DISABLE）
    pthread_setcancelstate  (PTHREAD_CANCEL_ENABLE      ,NULL);
    //设置线程接受取消信号后的取消类型，PTHREAD_CANCEL_DEFERRED-到取消点再执行退出，PTHREAD_CANCEL_ASYNCHRONOUS-立即退出（PTHREAD_CANCEL_DEFERRED，PTHREAD_CANCEL_ASYNCHRONOUS）
    pthread_setcanceltype   (PTHREAD_CANCEL_DEFERRED    ,NULL);
    //部分阻塞的系统调用本身即为取消点

    int count=0;
    int data_length=0;

    pthread_cleanup_push((void *)thread_function_C_clean_up_1,"CLEAN-1");
    pthread_cleanup_push((void *)thread_function_C_clean_up_1,"CLEAN-2");
    pthread_cleanup_push((void *)thread_function_C_clean_up_2,"CLEAN-1");
    pthread_cleanup_push((void *)thread_function_C_clean_up_2,"CLEAN-2");
    printf("THREAD_C-%016lu-START--\r\n",pthread_self());

    while (count<48000)
    {
        /* code */
        const pthread_t  p_id=pthread_self();
        if(MUTEX_ENABLE)
        {
            pthread_mutex_lock(&mutex_C);
        }
        const int length=strlen(data);
        if(PRINT_DATA)
        {
            printf("THREAD_C-%16lu-DATA:-_-%010d-_-%s\r\n",p_id,count,data);
        }
        if(length!=data_length)
        {
            printf("THREAD_C-DATA-LENGTH-CHANGED:%d\r\n",length);
            data_length=length;
        }
        if(MUTEX_ENABLE)
        {
            pthread_mutex_unlock(&mutex_C);
        }
        //手动添加取消点，本线程设置为取消点执行取消测试，但上面的printf和strlen等系统调用也是取消点，锁资源有可能在取消时未释放
        pthread_testcancel();    
        count++;
        usleep(1);
    }   

    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);

    return count;
}

void thread_function_A_clean_up_1(void * arg)
{
    if (arg!=NULL)
    {
        printf("A_clean-1:%s\r\n",(char *) arg);
    }
    else
    {
        printf("A_clean-1:-NULL-\r\n");
    }
}
void thread_function_A_clean_up_2(void * arg)
{
    if (arg!=NULL)
    {
        printf("A_clean-2:-PID-%016lu-__-%s\r\n",pthread_self(),(char *) arg);
    }
    else
    {
        printf("A_clean-2:-PID-%016lu-__--NULL-\r\n",pthread_self());
    }
    if(CANCLE_CLEAR_ENABLE)
    {
        pthread_mutex_unlock(&mutex_A);
    }

}

void thread_function_B_clean_up_1(void * arg)
{
    if (arg!=NULL)
    {
        printf("B_clean-1:%s\r\n",(char *) arg);
    }
    else
    {
        printf("B_clean-1:-NULL-\r\n");
    }
}
void thread_function_B_clean_up_2(void * arg)
{
    if (arg!=NULL)
    {
        printf("B_clean-2:-PID-%016lu-__-%s\r\n",pthread_self(),(char *) arg);
    }
    else
    {
        printf("B_clean-2:-PID-%016lu-__--NULL-\r\n",pthread_self());
    }
    if(CANCLE_CLEAR_ENABLE)
    {
        pthread_mutex_unlock(&mutex_B);
    }
}
void thread_function_C_clean_up_1(void * arg)
{
    if (arg!=NULL)
    {
        printf("C_clean-1:%s\r\n",(char *) arg);
    }
    else
    {
        printf("C_clean-1:-NULL-\r\n");
    }
}
void thread_function_C_clean_up_2(void * arg)
{
    if (arg!=NULL)
    {
        printf("C_clean-2:-PID-%016lu-__-%s\r\n",pthread_self(),(char *) arg);
    }
    else
    {
        printf("C_clean-2:-PID-%016lu-__--NULL-\r\n",pthread_self());
    }
    if(CANCLE_CLEAR_ENABLE)
    {
        pthread_mutex_unlock(&mutex_C);
    }
}